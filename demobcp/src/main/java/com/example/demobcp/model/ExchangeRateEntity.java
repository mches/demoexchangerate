package com.example.demobcp.model;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "TBL_EXCHANGE_RATE")
public class ExchangeRateEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@Column(name = "currency_code_origin")
	private String currencyCodeOrigin;
	@Column(name = "currency_code_destination")
	private String currencyCodeDestination;
	@Column(name = "rate_c")
	private BigDecimal rateC;
	@Column(name = "rate_v")
	private BigDecimal rateV;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getCurrencyCodeOrigin() {
		return currencyCodeOrigin;
	}
	public void setCurrencyCodeOrigin(String currencyCodeOrigin) {
		this.currencyCodeOrigin = currencyCodeOrigin;
	}
	public String getCurrencyCodeDestination() {
		return currencyCodeDestination;
	}
	public void setCurrencyCodeDestination(String currencyCodeDestination) {
		this.currencyCodeDestination = currencyCodeDestination;
	}
	public BigDecimal getRateC() {
		return rateC;
	}
	public void setRateC(BigDecimal rateC) {
		this.rateC = rateC;
	}
	public BigDecimal getRateV() {
		return rateV;
	}
	public void setRateV(BigDecimal rateV) {
		this.rateV = rateV;
	}
	
	@Override
	public String toString() {
		return "ExchangeRateEntity [id=" + id + ", currencyCodeOrigin=" + currencyCodeOrigin
				+ ", currencyCodeDestination=" + currencyCodeDestination + ", rateC=" + rateC + ", rateV=" + rateV
				+ "]";
	}
	
	


}
