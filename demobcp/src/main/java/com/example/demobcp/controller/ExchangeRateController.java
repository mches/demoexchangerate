package com.example.demobcp.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.example.demobcp.beans.ExchangeRateRequest;
import com.example.demobcp.beans.ExchangeRateResponse;
import com.example.demobcp.model.ExchangeRateEntity;
import com.example.demobcp.service.ConvertDolares;
import com.example.demobcp.service.ConvertEuros;
import com.example.demobcp.service.ConvertSoles;
import com.example.demobcp.service.ExchangeRateService;
import com.example.demobcp.service.MoneyCalculator;

@RestController
@RequestMapping("/exchangerate")
public class ExchangeRateController {
	
	@Autowired
	private MoneyCalculator  calculator;

	@Autowired
	private ExchangeRateService exchangeRateService;
	
	@Autowired
	private ConvertSoles  convertSoles;
	
	@Autowired
	private ConvertDolares  convertDolares;
	
	@Autowired
	private ConvertEuros  convertEuros;
	
	@RequestMapping(value = "/save", method = RequestMethod.POST, produces = { MediaType.APPLICATION_JSON_UTF8_VALUE })
	@ResponseStatus(value = HttpStatus.OK)
    public @ResponseBody ResponseEntity<ExchangeRateEntity> saveExchangeRate(@RequestBody final ExchangeRateEntity employee) {
		ExchangeRateEntity exchangeRateEntity = exchangeRateService.saveExchangeRate(employee);
	        return new ResponseEntity<ExchangeRateEntity>(exchangeRateEntity, new HttpHeaders(), HttpStatus.OK);
    }
	
	@RequestMapping(value = "/money", method = RequestMethod.POST, produces = { MediaType.APPLICATION_JSON_UTF8_VALUE })
	@ResponseStatus(value = HttpStatus.OK)
    public @ResponseBody ExchangeRateResponse moneyExchange(@RequestBody final ExchangeRateRequest request) throws Exception {
		
		ExchangeRateResponse  response = new ExchangeRateResponse();
		
		switch (request.getCurrencyDestination()) {
		case "604":
			response=calculator.calculate(request,convertSoles);
			break;
		case "840":
			response=calculator.calculate(request,convertDolares);
			break;

		default:
			response=calculator.calculate(request,convertEuros);
			break;
		}
		
		return  response;
    }
	
	@GetMapping
    public ResponseEntity<List<ExchangeRateEntity>> getAllEmployees() {
        List<ExchangeRateEntity> list = exchangeRateService.getAllExchangeRate();
        return new ResponseEntity<List<ExchangeRateEntity>>(list, new HttpHeaders(), HttpStatus.OK);
    }
	
	

}
