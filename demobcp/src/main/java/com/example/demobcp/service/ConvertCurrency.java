package com.example.demobcp.service;

import com.example.demobcp.beans.ExchangeRateRequest;
import com.example.demobcp.beans.ExchangeRateResponse;

public interface ConvertCurrency {
	
	ExchangeRateResponse convert(ExchangeRateRequest request);

}
