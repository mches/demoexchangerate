package com.example.demobcp.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demobcp.beans.ExchangeRateRequest;
import com.example.demobcp.beans.ExchangeRateResponse;

@Service
public class ConvertDolares implements ConvertCurrency {
	
	@Autowired
	private ExchangeRateService exchangeRateService;

	private ExchangeRateResponse response;

	@Override
	public ExchangeRateResponse convert(ExchangeRateRequest request) {	
		
		exchangeRateService.getExchangeRate(request.getCurrencyOrigin(), request.getCurrencyDestination())
		.map(x -> new ExchangeRateResponse(request.getAmount() / x.getRateV().doubleValue(), x.getRateV().doubleValue()))
		.subscribe(x -> response = x);

		if (response != null) {
			response.setAmount(request.getAmount());
			response.setCurrencyOrigin(request.getCurrencyOrigin());
			response.setCurrencyDestination(request.getCurrencyDestination());
		}

		return response;
	}


}
