package com.example.demobcp.service;

import java.util.List;

import com.example.demobcp.model.ExchangeRateEntity;

import rx.Observable;

public interface ExchangeRateService {
	
	Observable<ExchangeRateEntity> getExchangeRate(String currencyCodeO,String currencyCodeD);
	ExchangeRateEntity saveExchangeRate(ExchangeRateEntity request);
	List<ExchangeRateEntity> getAllExchangeRate();

}
