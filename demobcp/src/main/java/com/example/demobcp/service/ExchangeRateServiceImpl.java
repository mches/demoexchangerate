package com.example.demobcp.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demobcp.model.ExchangeRateEntity;
import com.example.demobcp.repository.ExchangeRateRepository;

import rx.Observable;

@Service
public class ExchangeRateServiceImpl implements ExchangeRateService {

	@Autowired
	private ExchangeRateRepository exchangeRateRepository;

	@Override
	public Observable<ExchangeRateEntity> getExchangeRate(String currencyCodeO,String currencyCodeD) {
		return Observable.from(exchangeRateRepository.findAll()).filter(x->x.getCurrencyCodeOrigin().equals(currencyCodeO) && x.getCurrencyCodeDestination().equals(currencyCodeD)||
																		   x.getCurrencyCodeOrigin().equals(currencyCodeD) && x.getCurrencyCodeDestination().equals(currencyCodeO));
	}

	@Override
	public ExchangeRateEntity saveExchangeRate(ExchangeRateEntity request) {
		// TODO Auto-generated method stub
		return exchangeRateRepository.save(request);
	}

	@Override
	public List<ExchangeRateEntity> getAllExchangeRate() {
		// TODO Auto-generated method stub
		return exchangeRateRepository.findAll();
	}

}
