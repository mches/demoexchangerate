package com.example.demobcp.service;

import org.springframework.stereotype.Component;

import com.example.demobcp.beans.ExchangeRateRequest;
import com.example.demobcp.beans.ExchangeRateResponse;

@Component
public class MoneyCalculator {

	

	public ExchangeRateResponse calculate(ExchangeRateRequest request,ConvertCurrency convertCurrency) throws Exception {
		return convertCurrency.convert(request);
	}

}
