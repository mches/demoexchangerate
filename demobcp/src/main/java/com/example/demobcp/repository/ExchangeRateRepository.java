package com.example.demobcp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.demobcp.model.ExchangeRateEntity;



@Repository
public interface ExchangeRateRepository extends JpaRepository<ExchangeRateEntity, Long>{

}
