package com.example.demobcp.beans;

import java.io.Serializable;

public class ExchangeRateResponse extends ExchangeRateRequest implements Serializable {

	public ExchangeRateResponse() {
		super();
	}

	public ExchangeRateResponse(double changeAmount, double rate) {
		super();
		this.changeAmount = changeAmount;
		this.rate = rate;
	}

	private static final long serialVersionUID = 1L;

	private double changeAmount;
	private double rate;

	public double getChangeAmount() {
		return changeAmount;
	}

	public void setChangeAmount(double changeAmount) {
		this.changeAmount = changeAmount;
	}

	public double getRate() {
		return rate;
	}

	public void setRate(double rate) {
		this.rate = rate;
	}

}
