package com.example.demobcp.beans;

import java.io.Serializable;

public class ExchangeRateRequest implements Serializable{

	private static final long serialVersionUID = 1L;
	private double amount;
	private String currencyOrigin;
	private String currencyDestination;

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public String getCurrencyOrigin() {
		return currencyOrigin;
	}

	public void setCurrencyOrigin(String currencyOrigin) {
		this.currencyOrigin = currencyOrigin;
	}

	public String getCurrencyDestination() {
		return currencyDestination;
	}

	public void setCurrencyDestination(String currencyDestination) {
		this.currencyDestination = currencyDestination;
	}

}
