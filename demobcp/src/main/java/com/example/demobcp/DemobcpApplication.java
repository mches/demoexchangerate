package com.example.demobcp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemobcpApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemobcpApplication.class, args);
	}

}
